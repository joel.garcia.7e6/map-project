package com.example.mapproject

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.fragment.findNavController
import com.example.mapproject.databinding.FragmentMarkerBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class MarkerFragment : Fragment() {
    lateinit var binding: FragmentMarkerBinding
    private val db = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    var imageUri: Uri? =null
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                imageUri = data.data!!
                binding.image.setImageURI(imageUri)
            }
        }
    }
    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncher.launch(intent)
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        binding= FragmentMarkerBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val latitud= arguments?.getFloat("latitude")
        val longitud= arguments?.getFloat("longitude")
        val imageURI= Uri.parse(arguments?.getString("imageUri"))
        binding.et1.setText(latitud.toString())
        binding.et2.setText(longitud.toString())
        binding.image.setImageURI(imageURI)

        fun imageToStorage(){
            val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
            val now = Date()
            val fileName = formatter.format(now)
            val storage1 = FirebaseStorage.getInstance().getReference("images/$fileName")
            storage1.putFile(imageURI!!)
                .addOnSuccessListener {
                    binding.image.setImageURI(null)
                    Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                }
            val storage = FirebaseStorage.getInstance().reference.child("images/2022_03_20_11_47_04")
            val localFile = File.createTempFile("temp", "jpeg")
            storage.getFile(localFile).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                binding.image.setImageBitmap(bitmap)


            }.addOnFailureListener{
                Toast.makeText(requireContext(), "Error downloading image!", Toast.LENGTH_SHORT)
                    .show()
            }

        }

        binding.cam.setOnClickListener(){
            val action = MarkerFragmentDirections.actionMarkerFragmentToCamaraFragment(latitud!!,longitud!!)
            findNavController().navigate(action)

        }
        binding.save.setOnClickListener {
            imageToStorage()
            db.collection("users").document(auth.currentUser?.email!!)
                .collection("marcadores").document(binding.et3.text.toString()).set(
                    hashMapOf("longitud" to longitud,
                        "latitud" to latitud,
                        "nombre" to binding.et3.text.toString(),
                        "image" to imageURI)
                )

            findNavController().navigate(R.id.action_markerFragment_to_fragment1)

        }

    }




}