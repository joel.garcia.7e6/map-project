package com.example.mapproject

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*

class Fragment2 : Fragment() {
    private val db = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    val listamarcadores= mutableListOf<Marcador>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_2, container, false)
    }
    private fun eventChangeListener()  {
        db.collection("users").document(auth.currentUser?.email!!).collection("marcadores").addSnapshotListener(object:
            EventListener<QuerySnapshot> {
            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                if(error != null){
                    Log.e("Firestore error", error.message.toString())
                    return
                }
                for(dc: DocumentChange in value?.documentChanges!!){
                    if(dc.type == DocumentChange.Type.ADDED){
                        val data = dc.document.data
                        val marcador=Marcador(
                            data["latitud"] as Double,
                            data["longitud"]as Double,
                            data["nombre"] as String,
                            data["image"] as String
                        )
                        listamarcadores.add(marcador)
                    }
                }
            }
        })


    }
    private fun setUpRecyclerView(lista: MutableList<Marcador>?){
        val myAdapter= lista.let { MarkerAdapter(it, this) }
        binding.recyclerView.apply {
            adapter=myAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventChangeListener()

    }

}