package com.example.mapproject


import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.mapproject.databinding.FragmentLogInBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class LogInFragment : Fragment() {

    lateinit var binding: FragmentLogInBinding
    private val db = FirebaseFirestore.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding= FragmentLogInBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.logButton.setOnClickListener{
            val email=binding.email.text.toString()
            val password=binding.password.text.toString()
            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(email,password)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        findNavController().navigate(R.id.action_logInFragment_to_fragment1)
                    }
                    else{
//                        showError("Error al fer login")
                    }
                }

        }

        binding.regButton.setOnClickListener {
            val email=binding.email.text.toString()
            val password=binding.password.text.toString()
            db.collection("users").document(email).set(
                hashMapOf("email" to email)
            )

            FirebaseAuth.getInstance().
            createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        findNavController().navigate(R.id.action_logInFragment_to_fragment1)
                    }
                    else{
//                        showError("Error al registrar l'usuari")
                    }
                }

        }
    }

}