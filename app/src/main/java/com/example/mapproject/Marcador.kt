package com.example.mapproject

class Marcador(
    val latitud:Double,
    val longitud:Double,
    val nombre:String,
    val foto: String
)