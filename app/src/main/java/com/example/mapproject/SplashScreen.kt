package com.example.mapproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        //Esconder ActionBar
        supportActionBar?.hide()

        //Coroutine
        CoroutineScope(Dispatchers.IO).launch {
            delay(2500L)
            startActivity(Intent(this@SplashScreen, MainActivity::class.java))
        }

    }

}
