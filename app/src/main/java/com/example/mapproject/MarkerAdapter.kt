package com.example.mapproject

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mapproject.databinding.MarkerBinding

class MarkerAdapter(private val marker: List<Marcador>, private val listener: MyOnClickListener):
    RecyclerView.Adapter<MarkerAdapter.ViewHolder>(){
        inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
            val binding = MarkerBinding.bind(view)
            fun setListener(marker: Marcador){
                binding.root.setOnClickListener {
                    listener.onClick(marker)
                }
            }

        }
        private lateinit var context: Context


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            context = parent.context
            val view = LayoutInflater.from(context).inflate(R.layout.marker, parent, false)
            return ViewHolder(view)
        }


        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val marcador = marker[position]
            with(holder) {
                setListener(marcador)
                binding.name.text = marcador.nombre

//                Glide.with(context)
//                    .load(marcador.foto)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .centerCrop()
//                    .circleCrop()
//                    .into(binding.photo)
            }

        }


        override fun getItemCount(): Int {
            return marker.size
        }
}